//importing node framework
var express = require('express');
var app = express();
var path = require('path');
//Respond with "hello world" for requests that hit our root "/"

app.use(express.static(path.resolve(__dirname + '/../public')));

/* app.get('/', function(req, res) {
  console.log(req.url);
  res.send('hello world test');
}); */
//listen to port 3000 by default
app.listen(process.env.PORT || 3000, () => {
  console.log(`server is listening on port 3000`);
});

module.exports = app;
