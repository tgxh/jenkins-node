FROM node:8.12.0

LABEL version="1.0" description="node1"

# 工作目录
WORKDIR /app

# 复制源码，仅复制服务端运行时需要的代码文件即可
COPY server/ /app/server/
COPY public/ /app/public/
# COPY package.json /app/
COPY ecosystem.config.yaml /app/

RUN cd /app/server && npm config set registry https://registry.npm.taobao.org && npm install --production

RUN npm install pm2 -g

# 镜像内的服务使用 3000 端口
EXPOSE 3000

# CMD [ "npm", "start" ]

# 使用pm2-docker启动程序
CMD ["pm2-runtime", "start", "ecosystem.config.yaml"]
