#!/bin/sh

img=nodetest
container=nodetest-container

currdir=$(pwd)

echo "build react start"

if [ ! -d "${currdir}/public" ]; then
    mkdir ${currdir}/public
fi

echo ">>> set registry start"
npm config list
npm config set registry https://registry.npm.taobao.org
npm config list
echo ">>> set registry end"

cd app && npm install && npm run build
cd ${currdir} && cp -rf ${currdir}/app/build/* ${currdir}/public/

echo "build react done"

docker stop ${container}
docker rm ${container}

echo "build docker start"

docker build -t $img .

echo "build docker done"

docker run --name ${container} -p 3000:3000 -d ${img}

echo "All done."